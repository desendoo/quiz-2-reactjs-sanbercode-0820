const volumeKubus = (sisi) => {
    return Math.pow(sisi, 3);
}

const volumeBalok = (panjang, lebar, tinggi) => {
    return panjang * lebar * tinggi;
}

console.log(`Volume kubus dengan sisi 3 adalah ${volumeKubus(3)}\nVolume balok dengan panjang = 3, lebar = 4, dan tinggi = 5 adalah ${volumeBalok(3, 4, 5)}`);