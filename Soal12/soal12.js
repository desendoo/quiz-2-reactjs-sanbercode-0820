class BangunDatar {
    constructor(nama) {
        this._nama = nama;
    }

    get nama() {
        return this._nama;
    }
    luas() {
        return "";
    }
    keliling() {
        return "";
    }
}

class Lingkaran extends BangunDatar {
    constructor(nama) {
        super(nama);
        this._jariJari;
    }
    
    set jariJari(jariJari) {
        this._jariJari = jariJari;
    }
    // phi * r x *
    luas() {
        return (22/7) * this._jariJari * this._jariJari;
    }
    // phi * 2 * jari-jari
    keliling() {
        return (22/7) * 2 * this._jariJari;
    }
}

class Persegi extends BangunDatar {
    constructor(nama) {
        super(nama);
        this._sisi;
    }
    
    set sisi(sisi) {
        this._sisi = sisi;
    }
    // sisi * sisi
    luas() {
        return this._sisi * this._sisi;
    }
    // 4 * sisi
    keliling() {
        return 4 * this._sisi;
    }
}

// ===== Proof =====
// Persegi
console.log("=== Persegi ===");
var persegi = new Persegi("Persegi suka-suka");
console.log(persegi.nama);
persegi.sisi = 15;
console.log("Luas persegi = " + persegi.luas()); // 255
console.log("Keliling persegi = " + persegi.keliling()); // 60

// Lingkaran
console.log("\n=== Lingkaran ===");
var lingkaran = new Lingkaran("Lingkaran Topi");
console.log(lingkaran.nama);
lingkaran.jariJari = 7;
console.log("Luas lingkaran = " + lingkaran.luas()); // 154
console.log("Keliling lingkaran = " + lingkaran.keliling()); // 44