let warna = ["biru", "merah", "kuning", "hijau"];

let dataBukuTambahan = {
    penulis: "John doe",
    tahunTerbit: 2020
}

let buku = {
    nama: "pemrograman dasar",
    jumlahHalaman: 172,
    warnaSampul: ["hitam"]
}

let hasilSpread = [...warna, dataBukuTambahan, buku];
console.log(hasilSpread);